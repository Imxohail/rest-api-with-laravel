$(function () {
    // Datepicker
    let dateToday = new Date();
    $("#datefield").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: dateToday,
    });

    // Validation
    $("#form").validate({
        rules: {
            "name": {
                required: true,
            },
            "description": {
                required: true,
            },
            'event-type': {
                required: true,
            },
            'participants[]': {
                required: true,
            },
            // 'organisers[]': {
            //     required: true,
            // },
            'event-date': {
                required: true,
            },
            'event-time': {
                required: true,
            },
            'location': {
                required: true,
            }
        },
        messages: {
            "name": {
                required: "Event title cannot be left blank",
            },
            'location': {
                required: "Event venue cannot be left blank",
            },
            'event-type': {
                required: "Event must be selected",
            },
            "description": {
                required: "Event description cannot be left blank",
            },
            "participants[]": {
                required: "Participant must be selected",
            },
            // "organisers[]": {
            //     required: "Organiser must be selected",
            // },
            'event-date': {
                required: "Event date must be selected",
            },
            'event-time': {
                required: "Event time must be selected",
            }
        },
        submitHandler: function (form) {
            $("#form").submit();
        }
    });

    // Distance and Time operation
    $("#information").on('click', function () {
        let destination = $("#location").val();
        if (destination) {
            $.ajax({
                method: 'POST',
                url: '/maps/getinfo',
                data: {'destination': destination},
                beforeSend: function () {
                    $('#info-display').html("Loading...");
                },
                success: function (response) {
                    if ((typeof (response['time']) != "undefined" && response['time'] !== null) && (typeof (response['distance']) != "undefined" && response['distance'] !== null)) {
                        $("#distance #time").val('');
                        $("#info-display").html("Time: <b>" + response['time'] + "</b> & Distance: <b>" + response['distance'] + "les</b>");
                        $("#distance").val(response['distance']);
                        $("#time").val(response['time']);
                    } else {
                        $("#distance #time").val('');
                        $("#info-display").html(response);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    $("#info-display").html(textStatus + ' : ' + errorThrown);
                }
            });
        } else {
            $("#info-display").html("Destination field is empty");
        }
    });

    // Loading Organisers on change of events
    $("#events").change(function () {
        let selectedEventType = $(this).children("option:selected").val();
        let organiserContainer = $("#organiser-container");
        selectedEventType == 0 ? organiserContainer.hide() : organiserContainer.show();

        $.ajax({
            method: 'POST',
            url: '/events/getorganisers',
            data: {'event_type_id': selectedEventType},
            success: function (response) {
                let html = '<label for="basic">Organisers:</label><select name="organisers[]" id="organisers-id" multiple="multiple" class="form-control">';
                $.each(response, function (index, value) {
                    html += '<option value="' + value['organiser_id'] + '">' + value['organiser_name'] + '</option>';
                });
                html += '</select>';
                $("#organiser-container").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });

    // Pass CSRF token globally for Ajax call
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Events grid via Data tables
    $('#grid').DataTable();
});