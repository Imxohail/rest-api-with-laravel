@extends('base')
@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Update an event</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
            @endif
            <form id="form" method="post" action="{{ route('events.update', $event->event_id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{ $event->name }}"/>
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" name="description" class="form-control" value="{{ $event->description }}"/>
                </div>

                <div class="form-group">
                    <label for="events">Event Type:</label>
                    <div>
                        <select id="events" name="event-type" class="required form-control">
                            <option value="">Select</option>
                            @foreach($eventTypes as $item)
                                <option {{ $event->event_type_id == $item['event_type_id'] ? "selected":"" }} value="{{$item['event_type_id']}}">{{$item['event_type_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

{{--                @isset($event->organisers)--}}
                    <div id="organiser-container" style="display: block" class="form-group">
                        <label for="basic">Organisers:</label>
                        <select name="organisers[]" id="organisers-id" multiple="multiple" class="form-control">
                            @foreach($organisers as $item)
                                @if($event->organisers != null)
                                    @if(in_array($item['organiser_id'], $event->organisers))
                                        <option selected value="{{$item['organiser_id']}}">{{$item['organiser_name']}}</option>
                                    @endif
                                @else
                                    <option value="{{$item['organiser_id']}}">{{$item['organiser_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
{{--                @endisset--}}

                @isset($event->participants)
                    <div class="form-group">
                        <label for="basic">Participants:</label>
                        <select name="participants[]" id="participants-id" multiple="multiple" class="form-control">
                            @foreach($participantTypes as $item)
                                @if(in_array($item['participant_type_id'], $event->participants))
                                    <option selected value="{{$item['participant_type_id']}}">{{$item['participant_type_name']}}</option>
                                @else
                                    <option value="{{$item['participant_type_id']}}">{{$item['participant_type_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @endisset

                <div class="form-group">
                    <label for="basic">Date:</label>
                    <input type="text" class="form-control" name="event-date" id="datefield" value="{{ $event->date }}">
                </div>

                <div class="form-group">
                    <label for="basic">Time:</label>
                    <input type="time" class="form-control" name="event-time" value="{{ $event->time }}">
                </div>

                <div class="form-group">
                    <label for="basic">Location:</label>
                    <input type="text" class="form-control" name="location" id="location" value="{{ $event->location }}">
                    <input type="hidden" value="" id="distance" name="distance">
                    <input type="hidden" value="" id="time" name="time">
                    <div id="info-display"></div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-info" id="information">Check Distance and Time</button>
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection