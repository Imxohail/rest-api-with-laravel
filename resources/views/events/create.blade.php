@extends('base')

@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Add an event</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form id="form" method="post" action="{{ route('events.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" name="description" class="form-control" value="{{ old('description') }}"/>
                    </div>

                    <div class="form-group">
                        <label for="events">Event Type:</label>
                        <div>
                            <select id="events" name="event-type" class="required form-control">
                                <option value="">Select</option>
                                @foreach($eventTypes as $item)
                                    <option {{ (\Illuminate\Support\Facades\Input::old("event-type") == $item['event_type_id'] ? "selected":"") }} value="{{$item['event_type_id']}}">{{$item['event_type_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="organiser-container" style="display: none" class="form-group"></div>

                    <div class="form-group">
                        <label for="basic">Participants:</label>
                        <select name="participants[]" id="participants-id" multiple="multiple" class="form-control">
                            @foreach($participantTypes as $item)
                                <option value="{{$item['participant_type_id']}}">{{$item['participant_type_name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="basic">Date:</label>
                        <input type="text" class="form-control" name="event-date" id="datefield"
                               value="{{ old('event-date') }}">
                    </div>

                    <div class="form-group">
                        <label for="basic">Time:</label>
                        <input type="time" class="form-control" name="event-time" value="{{ old('event-time') }}">
                    </div>

                    <div class="form-group">
                        <label for="basic">Location:</label>
                        <input type="text" class="form-control" name="location" id="location"
                               value="{{ old('location') }}">
                        <input type="hidden" value="" id="distance" name="distance">
                        <input type="hidden" value="" id="time" name="time">
                        <div id="info-display"></div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="information">Check Distance and Time</button>
                    </div>

                    <button type="submit" id="submit-button" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
