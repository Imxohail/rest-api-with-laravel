@extends('base')
<div>
    <a style="margin: 19px;" href="{{ route('events.create')}}" class="btn btn-primary">New event</a>
</div>
@section('main')
    <div id="app" class="row">
        <div class="col-sm-12">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="col-sm-12">
        <h1 class="display-3">Events</h1>
        <table id="grid" class="display" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Event type</th>
                <th>Participants</th>
                <th>Organisers</th>
                <th>Event date</th>
                <th>Event time</th>
                <th>Venue</th>
                <th>Distance</th>
                <th>Duration</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
                <tr>
                    <td>{{$event['name']}}</td>
                    <td>{{$event['event_type']}}</td>
                    <td>{{$event['participants']}}</td>
                    <td>{{$event['organisers']}}</td>
                    <td>{{$event['date']}}</td>
                    <td>{{$event['time']}}</td>
                    <td>{{$event['location']}}</td>
                    @if(isset($event['venue_distance']))
                        <td>{{$event['venue_distance']."les"}}</td>
                    @else
                        <td>null</td>
                    @endif
                    @if(isset($event['venue_travel_time']))
                        <td>{{$event['venue_travel_time']}}</td>
                    @else
                        <td>null</td>
                    @endif
                    <td>
                        <a href="{{ route('events.edit',$event['event_id'])}}" >Edit</a>
                        <form id="destroy" action="{{ route('events.destroy', $event['event_id'])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
@endsection