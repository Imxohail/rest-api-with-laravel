<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sentral Education</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/dataTables.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="container">
    @yield('main')
</div>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/additional-methods.js') }}"></script>
<script src="{{ asset('js/dataTables.js') }}"></script>
<script src="{{ asset('js/app.js') }}" type="text/js"></script>
<script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
</body>
</html>