<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Default route
Route::redirect('/', '/events');

// List of events
Route::resource('events', 'EventController');

// Get organisers select drop down via Ajax call
Route::post('/events/getorganisers', 'EventController@getOrganisers');

// Retrieve distance and duration from origin to destination
Route::post('/maps/getinfo', 'MapsController@getInfo');
