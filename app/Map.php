<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    /**
     * Calculates the distance and travel time between two address
     *
     * @param $address_to
     * @param bool $address_from
     * @return mixed
     */
    public static function getDistance($address_to, $address_from = false)
    {
        // If addressFrom is not set, use the school address from environment variables
        $address_from = ($address_from === false) ? $_ENV["SCHOOL_ADDRESS"] : $address_from;

        // Change address format
        $formatted_addr_from = str_replace(' ', '+', $address_from);
        $formatted_addr_to = str_replace(' ', '+', $address_to);

        // Geocoding API request with start address
        $geo_distance_matrix = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . $formatted_addr_from . '&destinations=' . $formatted_addr_to . '&key=' . $_ENV["GOOGLE_MAPS_DISTANCE_API_KEY"]);
        $response = json_decode($geo_distance_matrix);

        // If status: error is returned
        if (!empty($response->error_message)) {
            return $response->error_message;
        } else if ($response->status === 'OK') {
            $result = array();
            foreach ($response->rows[0]->elements as $map) {
                if ($map->status == "OK") {
                    $result['time'] = $map->duration->text;
                    $result['distance'] = $map->distance->text;
                } else if ($map->status == "NOT_FOUND") {
                    return "Destination address not found";
                } else {
                    return "Unknown error";
                }
            }
            return $result;
        }
    }

    /**
     * Converts seconds to H:M:S format
     *
     * @param $seconds
     * @return string
     */
    public function getTimeFormat($seconds)
    {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);

        return $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
    }
}
