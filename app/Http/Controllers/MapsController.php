<?php

namespace App\Http\Controllers;

use App\Map;
use Illuminate\Http\Request;

class MapsController extends Controller
{
    /**
     * Handler to return distance and travelling time
     *
     * @param NULL $location
     * @return array
     */
    public function getInfo(Request $request)
    {
        $destination = $request->destination;

        // Get distance and duration to destination
        $response = Map::getDistance($destination);

        return $response;
    }
}
