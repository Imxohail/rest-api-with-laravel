<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Organiser;
use App\ParticipantType;
use Illuminate\Http\Request;
use App\Event;


class EventController extends Controller
{
    /**
     * Display a listing of the events.test
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // All events
        $events = Event::getEventsForView();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new event.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        // Event types
        $data['eventTypes'] = EventType::all()->toArray();

        // Event organisers
        $data['organisers'] = Organiser::all()->toArray();

        // Event participant types
        $data['participantTypes'] = ParticipantType::all()->toArray();

        return view('events.create', ["eventTypes" => $data['eventTypes'], "organisers" => $data['organisers'],
            "participantTypes" => $data['participantTypes']]);
    }

    /**
     * Store a newly created event in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form validation
        $request->validate(
            [
            'name' => 'required',
            'description' => 'required',
            'event-type' => 'required',
            'event-date' => 'required',
            'event-time' => 'required',
            'location' => 'required',
            'participants' => 'required',
            ]
        );

        // Get the distance and travelling time between school and venue
        $response = Event::getDistanceAndTime($request);

        // Create new event on successful validation
        $event = new Event(
            [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'event_type_id' => $request->get('event-type'),
            'organisers' => json_encode($request->get('organisers')),
            'participants' => json_encode($request->get('participants')),
            'date' => $request->get('event-date'),
            'time' => $request->get('event-time'),
            'location' => $request->get('location'),
            'venue_distance' => isset($response['distance']) ? $response['distance'] : null,
            'venue_travel_time' => isset($response['time']) ? $response['time'] : null,
            ]
        );
        $event->save();

        return redirect('/events')->with('success', 'Event saved!');
    }

    /**
     * Display the specified event.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get specific event by id
        $event = Event::find($id);

        // JSON decode participants and organisers
        $event->participants = json_decode($event->participants);
        $event->organisers = json_decode($event->organisers);

        $data = array();

        // All event types
        $data['eventTypes'] = EventType::all()->toArray();

        // Get organisers based on event's type
        $data['organisers'] = Organiser::find(['organiser_type_id' => $event->event_type_id])->toArray();

        // All participant types
        $data['participantTypes'] = ParticipantType::all()->toArray();

        return view('events.edit', ["event" => $event, "eventTypes" => $data['eventTypes'], "organisers" => $data['organisers'], "participantTypes" => $data['participantTypes']]);
    }

    /**
     * Update the specified event in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Form validation
        $request->validate(
            [
            'name' => 'required',
            'description' => 'required',
            'event-type' => 'required',
            'event-date' => 'required',
            'event-time' => 'required',
            'location' => 'required',
            'participants' => 'required',
            ]
        );

        // Get the distance and travelling time between school and venue
        $response = Event::getDistanceAndTime($request);

        $date = $request->get('event-date');

        // Convert cached date values from form's date field (local computer)
        if (isset($date)) {
            $date = strtr($request->get('event-date'), '/', '-');
        }

        // Update the event
        $event = Event::find($id);
        $event->name = $request->get('name');
        $event->description = $request->get('description');
        $event->event_type_id = $request->get('event-type');
        $event->organisers = json_encode($request->get('organisers'));
        $event->participants = json_encode($request->get('participants'));
        $event->date = $date;
        $event->time = $request->get('event-time');
        $event->location = $request->get('location');
        $event->venue_distance = isset($response['distance']) ? $response['distance'] : null;
        $event->venue_travel_time = isset($response['time']) ? $response['time'] : null;
        $event->save();

        return redirect('/events')->with('success', 'Event updated!');
    }

    /**
     * Remove the specified event from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get event object
        $event = Event::find($id);

        // Delete the event
        $event->delete();

        return redirect('/events')->with('success', 'Event deleted!');
    }

    /**
     * Ajax request for the organiser based on event type
     *
     * @param  Request $request
     * @return array
     */
    public function getOrganisers(Request $request)
    {
        // Organisers for the event type selected
        $organisers = Organiser::where('organiser_type_id', $request->event_type_id)->get();

        return $organisers;
    }
}