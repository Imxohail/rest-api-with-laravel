<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organiser extends Model
{
    protected $primaryKey = 'organiser_id';
}
