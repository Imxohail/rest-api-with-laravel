<?php

namespace App;

use App\Http\Controllers\MapsController;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $primaryKey = 'event_id';

    protected $fillable = [
        'name',
        'description',
        'event_type_id',
        'organisers',
        'participants',
        'date',
        'time',
        'location',
        'venue_distance',
        'venue_travel_time'
    ];

    /**
     * Retrieve all saved events
     * Overwrite events, participants and organisers ids with name in the fetched array
     *
     * @return array
     */
    public static function getEventsForView()
    {
        $events = Event::all()->toArray();

        foreach ($events as &$event) {
            // Overwrite event ids with events names
            $event['event_type'] = EventType::where('event_type_id', $event['event_type_id'])->first()->event_type_name;

            // Overwrite participant ids with participant names
            $participants = json_decode($event['participants']);
            foreach ($participants as &$participant_id) {
                $participant_id = ParticipantType::where('participant_type_id', $participant_id)->first()->participant_type_name;
            }
            $event['participants'] = implode(',', $participants);

            // If organisers were assigned
            if ($event['organisers'] != "null") {
                // Overwrite organiser ids with organisers names
                $organisers = json_decode($event['organisers']);
                foreach ($organisers as &$organiser_id) {
                    $organiser_id = Organiser::where('organiser_type_id', $organiser_id)->first()->organiser_name;
                }
                $event['organisers'] = implode(',', $organisers);
            }
        }

        return $events;
    }

    /**
     * If distance and time sent via form submit use them
     * otherwise send new request to Google maps
     *
     * @param $request
     * @return array
     */
    public static function getDistanceAndTime($request)
    {
        $response = array();

        // If distance and time hidden input fields were not set on form submission
        // Get duration and time from google
        if ($request->get('distance') === null && $request->get('time') === null) {
            $maps = new MapsController();
            $request->destination = $request->get('location');
            $response = $maps->getInfo($request);
        } else {
            $response['distance'] = $request->get('distance');
            $response['time'] = $request->get('time');
        }

        return $response;
    }
}
