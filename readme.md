### A CRUD based API for school events.

## TechStack
	Homestead
	PHP 7.3.5
	Ubuntu 18.04
	Laravel 5.8
	Composer (for PHP dependencies)
	Node (for front-end dependencies)
	Webpack/Laravel Mixin for bundles
	Jquery validator
	Jquery
	DataTables
	MySQL
	Google Maps Javascript API
	php unit testing
	
## Setup
	Install VirtualBox
	Install Vagrant
	Add Homestead vagrant box by using "vagrant box add laravel/homestead"
	Clone Homestead on local system by using "git clone https://github.com/laravel/homestead.git ~/Homestead"
	cd ~/Homestead
	git checkout <latest stable version>
	bash init.sh // to create a Homestead.yaml file
	cd // move back to parent
	Clone my laravel repository using "git clone git@bitbucket.org:Imxohail/laravel.git" 
	cd ~/Homestead
	pico Homestead.yaml
	
	My Homestead.yaml contents:
---
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/laravel // my local folder
      to: /home/vagrant/laravel // mapped to folder on Homestead

sites:
    - map: homestead.app
      to: /home/vagrant/laravel/public

databases:
    - homestead
---

## Run
	After saving, run "vagrant up"
	Once Homestead is up and running
	run "vagrant ssh"
	cd laravel
	php artisan migration // runs all migrations
	php artisan db:seed  // runs all seeds
	composer install // all php dependencies from composer.json
	npm install // all frontend dependencies from package.json

	Site sould be available on: http://192.168.10.10
	
## .env file
	Add these two lines at the end of .env file
	GOOGLE_MAPS_DISTANCE_API_KEY=<YOUR KEY>
	SCHOOL_ADDRESS="Macquarie Centre, Herring Rd &, Waterloo Rd, North Ryde NSW 2113" // it can be changed to desired location

