<?php

use Illuminate\Database\Seeder;

class OrganiserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organisers')->truncate();

        $organisers = ['Book fair organiser', 'Concert organiser', 'Camping organiser'];
        foreach($organisers as $key => $value){
            DB::table('organisers')->insert([
                'organiser_name' => $value,
                'organiser_type_id' => $key+1,
            ]);
        }
    }
}
