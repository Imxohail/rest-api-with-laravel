<?php

use Illuminate\Database\Seeder;

class OrganiserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organiser_types')->truncate();

        $organiserData = ['Book fair', 'Concert', 'Camping'];
        foreach($organiserData as $value){
            DB::table('organiser_types')->insert([
                'organiser_type_name' => $value,
            ]);
        }
    }
}
