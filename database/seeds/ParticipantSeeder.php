<?php

use Illuminate\Database\Seeder;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participants')->truncate();

        $participants = ['Tim', 'Joy', 'David', 'Nick'];
        foreach($participants as $key => $value){
            DB::table('participants')->insert([
                'participant_name' => $value,
                'participant_type_id' => $key+1,
            ]);
        }
    }
}
