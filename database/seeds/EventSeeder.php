<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->truncate();

        DB::table('events')->insert([
        'name' => 'Concert',
            'description' => 'Musical Concert',
            'event_type_id' => 2,
            'organisers' => '["2"]',
            'participants' => '["3"]',
            'date' => '2019-12-31',
            'time' => '00:00',
            'location' => 'Sydney Opera House, Bennelong Point, Sydney NSW 2000',
            'venue_distance' => '10.6 mi',
            'venue_travel_time' => '20 mins',
        ]);

        DB::table('events')->insert([
            'name' => 'Book show',
            'description' => 'Book show',
            'event_type_id' => 1,
            'organisers' => '["1"]',
            'participants' => '["3"]',
            'date' => '2019-12-26',
            'time' => '00:00',
            'location' => 'Sydney Opera House, Bennelong Point, Sydney NSW 2000',
            'venue_distance' => '10.6 mi',
            'venue_travel_time' => '20 mins',
        ]);

        DB::table('events')->insert([
            'name' => 'Camp',
            'description' => 'camping',
            'event_type_id' => 3,
            'organisers' => '["3"]',
            'participants' => '["2","3"]',
            'date' => '2019-12-12',
            'time' => '00:00',
            'location' => 'Sydney Opera House, Bennelong Point, Sydney NSW 2000',
            'venue_distance' => '10.6 mi',
            'venue_travel_time' => '20 mins',
        ]);
    }
}
