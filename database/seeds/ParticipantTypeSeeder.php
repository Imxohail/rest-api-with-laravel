<?php

use Illuminate\Database\Seeder;

class ParticipantTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participant_types')->truncate();

        $participantData = ['Student', 'Staff', 'Parent', 'Volunteer'];
        foreach($participantData as $value) {
            DB::table('participant_types')->insert([
                'participant_type_name' => $value,
            ]);
        }
    }
}
