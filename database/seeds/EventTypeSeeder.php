<?php

use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_types')->truncate();

        $event_types = ['Book fair', 'Concert', 'Camping'];
        foreach($event_types as $key => $value){
            DB::table('event_types')->insert([
                'event_type_name' => $value,
                'event_type_description' => $key+1,
            ]);
        }
    }
}
